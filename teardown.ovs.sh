#!/bin/bash

# get input.
ovs_name=$1
if [[ -z $ovs_name ]]; then
	echo "Usage: $0 <ovs name> "
	echo "e.g. $0 ovsbr0"
	exit 1
fi

# get ports in this bridge.
ports=`ovs-vsctl list-ports $ovs_name`
if [[ -z $ports ]]; then 
	ovs-vsctl del-br $ovs_name
	exit $?
fi

# is dhcp ?
dhcp_pids=`ps aux | grep "dhclient $ovs_name" | grep -v "grep" | awk -F ' ' '{print $2}'`

# Not dhcp. 
if [[ -z $dhcp_pids ]]; then 
	# get interface ip address and netmask.
	int_ip=`ifconfig $ovs_name | grep "inet addr" | awk -F ' ' '{print $2}'| awk -F ':' '{print $2}'`
	int_mask=`ifconfig $ovs_name | grep "Mask" | awk -F ' ' '{print $4}' | awk -F ':' '{print $2}'`
	# delete bridge. 
	ovs-vsctl del-br $ovs_name
	if [[ $? -ne 0 ]]; then 
		echo "Delete bridge failed: $ovs_name"
		exit 1
	fi
	# restore interfaces.
	for port in $ports; do 
		ifconfig $port $int_ip netmask $int_mask up	
	done	
# it's dhcp
else
	# delete bridge. 
	ovs-vsctl del-br $ovs_name
	if [[ $? -ne 0 ]]; then 
		echo "Delete bridge failed: $ovs_name"
		exit 1
	fi
	# restore interfaces.
	for port in $ports; do 
		dhclient $port	
	done
	# kill old dhclient.
	for dhcp_pid in $dhcp_pids; do
		kill $dhcp_pid
	done	
fi

# show results
ovs-vsctl show
echo "--------------------------------------------"
for port in $ports; do
	ifconfig $port
done 


