#!/bin/bash

brd=$1
dataPathID=$2
if [[ -z $dataPathID ]]; then
    echo "Usage: $0 <bridge name> <datapathID>"
    echo "E.g., $0 ovs-lan 0000000000000001"
    exit 1
fi
# Validation
ovs-vsctl show | grep "$brd" | grep "Bridge" 
if [[ $? -ne 0 ]]; then
    echo "Could not find bridge: $1"
    exit 1
fi
ovs-vsctl set bridge $brd other-config:datapath-id=$dataPathID


