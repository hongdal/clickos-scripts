# This script provides with am example to set a throughput control 
# on the network interfaces. 
# Using tc ... 
# 
if [[ $# -ne 1 ]]; then
	echo "Options: ./router-setup.sh [pfifo | ared | codel | fq_codel]"
fi

sudo ethtool --offload eth2 tx off rx off tso off gso off gro off lro off ufo off rxvlan off txvlan off sg off

sudo ethtool --offload eth3 tx off rx off tso off gso off gro off lro off ufo off rxvlan off txvlan off sg off

sudo sysctl -w net.ipv4.ip_forward=1
sudo sysctl -w net.ipv4.tcp_congestion_control=reno
sudo sysctl -w net.ipv4.tcp_sack=1


#sudo tc qdisc del root dev eth2
sudo tc qdisc del root dev eth3

echo "Creating QDisc of type [$1]..."

#set up a HTB with a pfifo queue (droptail) holding 100 packets at 10mbps
#or with adaptive red, codel, or fq_codel
#dont pass in an option to use the default pfifo-fast
if [[ $# -eq 1 ]]; then 
	if [ $1 == 'pfifo' ]; then
		sudo tc qdisc add dev eth3 root handle 1: htb default 1
		sudo tc class add dev eth3 parent 1: classid 1:1 htb rate 38mbit
		sudo tc qdisc add dev eth3 parent 1:1 handle 2:0 pfifo limit 1024
	elif [ $1 == 'ared' ]; then
		sudo tc qdisc add dev eth3 root handle 1: htb default 1
		sudo tc class add dev eth3 parent 1: classid 1:1 htb rate 38mbit
		#sudo tc qdisc add dev eth0 parent 1:1 handle 10: red limit 90000 min 30000 max 80000 avpkt 900 burst 51 harddrop adaptive bandwidth 10mbit probability 0.1
		sudo tc qdisc add dev eth3 parent 1:1 handle 10: red limit 1843200 min 92160 max 921600 avpkt 900 burst 103 harddrop adaptive bandwidth 38mbit probability 0.1
	elif [ $1 == 'codel' ]; then
		sudo tc qdisc add dev eth3 root handle 1: htb default 1
		sudo tc class add dev eth3 parent 1: classid 1:1 htb rate 38mbit
		sudo tc qdisc add dev eth3 parent 1:1 handle 10: codel limit 600 target 0.02s interval 0.1
	elif [ $1 == 'fq_codel' ]; then
		sudo tc qdisc add dev eth3 root handle 1: htb default 1
		sudo tc class add dev eth3 parent 1: classid 1:1 htb rate 8mbit
		sudo tc qdisc add dev eth3 parent 1:1 handle 10: fq_codel limit 600
	fi
else
	echo "Qdisc not found, creating default pfifo-fast"
fi

