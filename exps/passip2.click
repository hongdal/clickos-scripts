// This file describe 2 flows of VNF.
// One flow deals with traffic comes in from eth0, to eth2; the other
// flow deals with traffic comes in from eth2, to eth0.
// Author: Hongda. 

// Definition of MACs. 
define ($eth2_dst 16:cc:26:dd:19:ca);   // ovs2:    ovs2-out
define ($eth2_src 00:00:00:00:02:02);   
define ($eth1_dst 00:8c:fa:5b:08:30);   // ovs-lan: client
define ($eth1_src 00:00:00:00:02:01);   
define ($eth0_dst 00:8c:fa:5b:09:9c);   // ovs-lan: ovs-lan 
define ($eth0_src 00:00:00:00:02:00);

// Definition of virtual devices. 
feth0 :: FromDevice(0);
teth0 :: ToDevice(0);
feth1 :: FromDevice(1);
teth1 :: ToDevice(1);
feth2 :: FromDevice(2);
teth2 :: ToDevice(2);

eth0 :: Classifier(
    0/000000000200 6/008cfa5b099c,    // traffic from eth0 
    0/000000000200 6/008cfa5b099c,    // traffic from eth0 
    -
);
eth0[2] -> Discard();
eth1 :: Classifier(
    0/000000000201,    // traffic for eth1 
    -
);
eth1[1] -> Discard();
eth2 :: Classifier(
    0/000000000202,    // traffic for eth2 
    -
);
eth2[1] -> Discard();

// Definition of VNF_IN
VNF_IN :: {
    input[0] -> PushNull -> [0]output; // eth0 -> eth1
    input[1] -> PushNull -> [1]output; // eth0 -> eth2
    input[2] -> PushNull -> [2]output; // eth1 -> eth0
    input[3] -> PushNull -> [3]output; // eth2 -> eth0
};
// Definition of VNF_OUT
VNF_OUT :: {
    input[0] -> StoreEtherAddress($eth1_dst, dst) -> StoreEtherAddress($eth1_src, src) -> [0]output;
    input[1] -> StoreEtherAddress($eth2_dst, dst) -> StoreEtherAddress($eth2_src, src) -> [1]output;
    input[2] -> StoreEtherAddress($eth0_dst, dst) -> StoreEtherAddress($eth0_src, src) -> [2]output;
    input[3] -> StoreEtherAddress($eth0_dst, dst) -> StoreEtherAddress($eth0_src, src) -> [3]output;
};


// from in device to VNF_IN, pushing ...
feth0 -> eth0[0] -> [0]VNF_IN
feth0 -> eth0[1] -> [1]VNF_IN
feth1 -> eth1[0] -> [2]VNF_IN
feth2 -> eth2[0] -> [3]VNF_IN
// from VNF_OUT to to_out_dev. 
VNF_OUT[0] -> teth1 
VNF_OUT[1] -> teth2 
VNF_OUT[2] -> teth0 
VNF_OUT[3] -> teth0 


// VNF CODE STARTS HERE
VNF_IN[0] -> [0]VNF_OUT // passthru
VNF_IN[1] -> [1]VNF_OUT // passthru
VNF_IN[2] -> [2]VNF_OUT // passthru
VNF_IN[3] -> [3]VNF_OUT // passthru
