// Definition of virtual devices.
feth0 :: FromDevice(0);
teth0 :: ToDevice(0);
feth1 :: FromDevice(1);
teth1 :: ToDevice(1);
feth2 :: FromDevice(2);
teth2 :: ToDevice(2);

// Definition of VNF_IN. Set dest MAC by default. 
VNF_IN :: {
    input[0] -> [0]output; // from eth0 
    input[1] -> [1]output; // from eth1 
    input[2] -> [2]output; // from eth2 
};

// Definition of VNF_OUT. set MAC address. 
VNF_OUT :: {
    input[0]->StoreEtherAddress(00:00:00:00:01:00,src)->StoreEtherAddress(34:17:eb:e5:85:3f,dst)->[0]output; // to eth0
    input[1]->StoreEtherAddress(00:00:00:00:01:01,src)->StoreEtherAddress(00:8c:fa:5b:09:9c,dst)->[1]output; // to eth1
    input[2]->StoreEtherAddress(00:00:00:00:01:02,src)->StoreEtherAddress(00:8c:fa:5b:09:9c,dst)->[2]output; // to eth2
};

// from in device to VNF_IN. & to VNF_OUT. 
feth0 -> [0]VNF_IN
feth1 -> [1]VNF_IN
feth2 -> [2]VNF_IN
// from VNF_OUT to out device. 
VNF_OUT[0] -> teth0 
VNF_OUT[1] -> teth1 
VNF_OUT[2] -> teth2 

define ($eth0_src 00:00:00:00:01:00);
define ($eth0_dst 34:17:eb:e5:85:3f);
define ($eth1_src 00:00:00:00:01:01);
define ($eth1_dst 00:8c:fa:5b:09:9c);
define ($eth2_src 00:00:00:00:01:02);
define ($eth2_dst 00:8c:fa:5b:09:9c);
//---------------------------------------------------//
//                VNF CODE STARTS HERE                
//---------------------------------------------------//
// dummy
Idle -> [0]VNF_OUT
VNF_IN[1] -> [1]VNF_OUT
VNF_IN[2] -> [2]VNF_OUT
Idle -> teth1
Idle -> teth2


define ($eth0_ip 130.127.133.2)
//--------------- Classify flows ----------------
traffic :: Classifier(
	12/0806 20/0001,    // arp 
	12/0800,            // ip
	-);
traffic[2] -> Discard();
VNF_IN[0] -> traffic;      // from eth0
//--------------- Redirecting.. -----------------

arpr :: ARPResponder($eth0_ip $eth0_src);

traffic[0] -> arpr -> teth0;    // to eth0, without destination.
traffic[1] -> CheckIPHeader(14) -> ICMPPingResponder() -> EtherMirror() -> teth0; // to eth0


