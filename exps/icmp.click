define ($IP 130.127.133.2);
//define ($IP 10.10.1.81);
define ($MAC 00:00:00:00:01:01);
define ($DNS 130.127.132.1);

source :: FromDevice(1);
sink   :: ToDevice(1);

c :: Classifier(
	12/0806 20/0001,    // arp 
//  12/0806 20/0002,    // arp
	12/0800,            // ip
	-);

//--------------- Redirecting.. -----------------
//TrafficOut :: {
//    input[0] -> StoreEtherAddress(00:00:00:00:01:00, src) -> StoreEtherAddress(ff:ff:ff:ff:ff:ff, dst) -> [0]output;
//}


arpr :: ARPResponder($IP $MAC);

source -> c;
//c[0] -> ARPPrint(OK) -> arpr -> StoreEtherAddress(34:17:eb:e5:85:3f, dst) -> StoreEtherAddress($MAC, src) -> sink;
c[0] -> arpr -> StoreEtherAddress($MAC, src) -> ARPPrint("OK", ETHER true) -> sink;
c[1] -> CheckIPHeader(14) -> ICMPPingResponder() -> EtherMirror() -> sink;
c[2] -> Discard;

// icmp sender. 
// ICMPPingSource($IP, $DNS) -> [0]TrafficOut[0] -> sink


