#!/bin/bash

echo $0

exp="^130.127.13[0-9].[0-9]*"
exp2=`ifconfig ovs-wan | grep "Mask" | awk '{print $2}' | awk -F ':' '{print $2}'`
if [[ $exp2 =~ $exp ]]; then
    echo "good"
else
    echo "bad"
fi

