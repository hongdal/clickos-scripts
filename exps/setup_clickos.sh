#!/bin/bash
# This file automatically generates 2 flows and 1 out-band control flow of VNF.
# The eth0 and eth1 are the normal traffic.
# The eth2 is the out-band control port.
#
# Author: Hongda. 

#-----------------------------------------------------------#
#                   Set parameters.                         # 
#-----------------------------------------------------------#
if [[ $# -lt 2 ]]; then 
    echo "Usage: $0 <sequence> <file.vnf> <xen_file_name> <host1> [host2] [host3] ... " >& 2 
    echo "e.g. $0 1 firewall.vnf example/firewall_01.xen 10.10.1.1 10.10.1.2 "  >& 2
    exit 1
fi

interface_number=2
counter=$1
vnf_file=$2
xen_path=$3
hosts=($4 $5 $6)
brds=()
eth_dsts=()
eth_srcs=()
counter=`expr $counter /  10``expr $counter % 10`
# possible ovs-bridges: 
ovs_brs=`ovs-vsctl show | grep "Bridge" | grep -v "\"" | awk '{print $2}'`

#-----------------------------------------------------------#
#                   Generate MACs                           # 
#-----------------------------------------------------------#
for index in `seq 0 $interface_number`; do 
    int_index=`expr $index / 10``expr $index % 10`
    eth_srcs[$index]=00:00:00:00:$counter:$int_index
    if [[ -z "${hosts[$index]}" ]]; then 
        # if not specify, use ovs-lan as default. 
        eth_dsts[$index]=`ifconfig ovs-lan | grep "HWaddr" | awk -F ' ' '{print $5}'`
        brds[$index]="ovs-lan"
        hosts[$index]=`ifconfig ovs-lan | grep "inet addr" | awk '{print $2}' | awk -F ':' '{print $2}'`
    else 
        for ovs_br in $ovs_brs; do 
            arp -d ${hosts[$index]} -i $ovs_br > /dev/null 
            ping -c 1 ${hosts[$index]} -I $ovs_br -W 1 > /dev/null
            echo "Finding MAC for host ${hosts[$index]} ... " >& 2
            eth_dsts[$index]=`arp -n | grep ${hosts[$index]} | grep $ovs_br | grep ":" | awk '{print $3}'`
            if [[ ! -z ${eth_dsts[$index]} ]]; then 
                brds[$index]=$ovs_br
                break 
            fi 
        done
        # Maybe it's a local IP, then find which ovs it belongs to. 
        if [[ -z ${eth_dsts[$index]} ]]; then
            for int in $(ifconfig | grep "HWaddr" | awk '{print $1}'); do
                ip=$(ifconfig $int | grep "net addr:${hosts[$index]} ")
                if [[ ! -z "$ip"  ]]; then
                    eth_dsts[$index]=$(ifconfig $int | grep "HWaddr" | awk -F ' ' '{print $5}')
                    # find int's vos-bridge.
                    brds[$index]=$(ovs-vsctl port-to-br $int 2>/dev/null)
                    # interface itself is a bridge. 
                    if [[ -z ${brds[$index]} ]]; then
                        brds[$index]=$int
                    fi
                fi
            done
        fi
    fi 
    if [[ -z "${eth_dsts[$index]}" ]]; then 
        echo "ERROR : Could not find MAC for host '${hosts[$index]}'" >& 2 
        exit 1
    else
        echo "MAC for host '${hosts[$index]}' found!" >& 2 
    fi
done
for index in `seq 0 $interface_number`; do
    echo "eth$index:" >& 2
    echo "    "${eth_srcs[$index]} >& 2
    echo "    "${eth_dsts[$index]} >& 2
    echo "-----------------------" >& 2 
done

#-----------------------------------------------------------#
#               Dump the basic VNF to stdout                # 
#-----------------------------------------------------------#
echo "// Definition of virtual devices." 
for index in `seq 0 $interface_number`; do 
    echo "feth$index :: FromDevice($index);"
    echo "teth$index :: ToDevice($index);"
done

echo ""
echo "// Definition of VNF_IN. Set dest MAC by default. "
echo "VNF_IN :: {"
for index in `seq 0 $interface_number`; do 
    echo "    input[$index] -> [$index]output; // from eth$index "
done
echo "};"

echo ""
echo "// Definition of VNF_OUT. set MAC address. "
echo "VNF_OUT :: {"
for index in `seq 0 $interface_number`; do 
    echo "    input[$index]->StoreEtherAddress(${eth_srcs[$index]},src)->StoreEtherAddress(${eth_dsts[$index]},dst)->[$index]output; // to eth$index"
done
echo "};"

echo ""
echo "// Classifier for interfaces. Avoid self loop"
for index in `seq 0 $interface_number`; do 
    echo "feth$index -> Classifier( 0/`echo ${eth_srcs[$index]} | tr -d :`)[0] -> [$index]VNF_IN;"
done

echo ""
echo "// from VNF_OUT to out device. "
for index in `seq 0 $interface_number`; do 
    echo "VNF_OUT[$index] -> teth$index "
done

#-----------------------------------------------------------#
#                           Add VNF.                        # 
#-----------------------------------------------------------#
# VNF_IN[i] --> comes from ethi. "
# VNF_OUT[i] --> goes to ethi. "
echo ""
# predefine variables for vnf_file. 
for index in `seq 0 $interface_number`; do
    echo "define (\$eth"$index"_src ${eth_srcs[$index]});"
    echo "define (\$eth"$index"_dst ${eth_dsts[$index]});"
done
echo "//---------------------------------------------------//"
echo "//                VNF CODE STARTS HERE                "
echo "//---------------------------------------------------//"
if [[ -f "$vnf_file" ]]; then
    cat $vnf_file
else 
    echo "WARNING: $vnf_file is not found, use default passthrough." >& 2
    echo "VNF_IN[0] -> [0]VNF_OUT // passthru"
    echo "VNF_IN[1] -> [1]VNF_OUT // passthru"
    echo "VNF_IN[2] -> Discard()  // discard"
fi

#-----------------------------------------------------------#
#                   Create Xen config                       # 
#-----------------------------------------------------------#
# Use base file name. 
fpath=${xen_path%/*} 
fname=${xen_path##*/}
fextend=${fname##*.}
fbase=${fname%.*}
name=$fbase
paras=()
for index in `seq 0 $interface_number`; do 
    paras[$(($index*2))]=${eth_srcs[$index]}
    paras[$(($index*2+1))]=${brds[$index]}
done
# invoke make_vnf_xen.sh
./make_nfv_xen.sh $name ${paras[*]} > "$xen_path" 

