#------------------------------------------------------------------------------
# This file records the evaluation results of 'coule_passip'
#   Setup: 
#       two clickos instances. Each of them is a one-way pass through functin.
#       client and server are able to communicate with each other through 
#       the two clickos instances. 
#
#                |------> passip_01 ----->|
#       clinet --|                        |--- server  
#                |<------ passip_02 <-----|
#
#       The underneath software switch is open Vswitch 2.3.1
#       The passip function does nothing to the packets except rewrite source 
#       and destination MACs to forward the packets.
#
#   Conducted at: 
#       07/13/2015. 
#------------------------------------------------------------------------------

root@client:/local/nfv-exp# netperf -H 10.10.1.2 -i 30 -t TCP_STREAM -- -s 54M -S 54M -m 1024
MIGRATED TCP STREAM TEST from 0.0.0.0 (0.0.0.0) port 0 AF_INET to 10.10.1.2 () port 0 AF_INET : +/-2.500% @ 99% conf. 
Recv   Send    Send                          
Socket Socket  Message  Elapsed              
Size   Size    Size     Time     Throughput  
bytes  bytes   bytes    secs.    10^6bits/sec  

425984 425984   1024    10.00     640.96   

