#!/bin/bash
# This file automatically generates 2 flows and 1 out-band control flow of VNF.
# The eth0 and eth1 are the normal traffic.
# The eth2 is the out-band control port.
#
# Author: Hongda. 

#-----------------------------------------------------------#
#                   Set parameters.                         # 
#-----------------------------------------------------------#



#-----------------------------------------------------------#
#               Output to the stdout.                       # 
#-----------------------------------------------------------#
echo ""
echo "define (\$eth0_dst $eth0_dst);   // host: $host1"
echo "define (\$eth0_src $eth0_src);   "
echo "define (\$eth1_dst $eth1_dst);   // host: $host2"
echo "define (\$eth1_src $eth1_src);   "
echo "define (\$eth2_dst $eth2_dst);   // host: $host3"
echo "define (\$eth2_src $eth2_src);   "

echo ""
echo "// Definition of virtual devices." 
echo "feth0 :: FromDevice(0);"
echo "teth0 :: ToDevice(0);"
echo "feth1 :: FromDevice(1);"
echo "teth1 :: ToDevice(1);"
echo "feth2 :: FromDevice(2);"
echo "teth2 :: ToDevice(2);"

echo ""
echo "// Definition of VNF_IN. Set dest MAC by default. "
echo "VNF_IN :: {"
echo "    input[0] -> StoreEtherAddress(\$eth1_dst, dst) -> [0]output; // eth0 -> eth1"
echo "    input[1] -> StoreEtherAddress(\$eth0_dst, dst) -> [1]output; // eth1 -> eth0"
echo "    input[2] -> StoreEtherAddress(\$eth2_dst, dst) -> [2]output; // eth2 -> eth2"
echo "};"

echo ""
echo "// Definition of VNF_OUT. set MAC address. "
echo "VNF_OUT :: {"
echo "    input[0] -> StoreEtherAddress(\$eth1_src, src) -> [0]output;"
echo "    input[1] -> StoreEtherAddress(\$eth0_src, src) -> [1]output;"
echo "    input[2] -> StoreEtherAddress(\$eth2_src, src) -> [2]output;"
echo "};"

echo ""
echo "// from in device to VNF_IN. "
echo "feth0 -> [0]VNF_IN"
echo "feth1 -> [1]VNF_IN"
echo "feth2 -> [2]VNF_IN"
echo "// from VNF_OUT to out device. "
echo "VNF_OUT[0] -> teth1 "
echo "VNF_OUT[1] -> teth0 "
echo "VNF_OUT[2] -> teth2 "

#-----------------------------------------------------------#
#               Output to the stdout.                       # 
#-----------------------------------------------------------#
echo ""
echo "// VNF CODE STARTS HERE"
if [[ -f "$vnf_file" ]]; then
    cat $vnf_file
else 
    echo "$vnf_file is not found, use default passthrough." >& 2
    echo "VNF_IN[0] -> [0]VNF_OUT // passthru"
    echo "VNF_IN[1] -> [1]VNF_OUT // passthru"
    echo "VNF_IN[2] -> Discard()  // discard"
fi 



