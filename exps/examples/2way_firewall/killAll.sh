#!/bin/bash

if [[ -z $2 ]]; then
    echo "Usage: $0 <base name > <number of instaces>"
    echo "E.g., $0 firewall 3  --> kill firewall_01, ... firewall_03"
    exit 1
fi

index=0
while [[ $index -lt $2 ]]; do 
    index=$(($index+1))
    xl destroy $1"_0"$index
done

xl list
