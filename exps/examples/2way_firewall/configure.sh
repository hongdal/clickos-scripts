#!/bin/bash

if [[ -z $1 ]]; then
    echo "Usage: $0 <num of instances (max 99)> [base name]"
    echo "E.g., $0 8 firewall  ---> create firewall_01, ... firewall_08 "
    exit 1
fi

base_name=$2
if [[ -z $base_name ]]; then
    base_name="firewall"
fi

# Generate *.click, *.xen 

# Creating instances.
index=0
while [[ $index -lt $1 ]]; do
    index=$(($index+1))
    seq=$index
    if [[ $seq -lt 10 ]]; then
        seq="0"$seq
    fi
    name=$base_name"_"$seq
    xl create $name".xen"
    sleep 0.3
    cosmos start $name $name".click"
    sleep 0.3
done

# Generate *.flows
ovs-ofctl del-flows ovs-lan 
index=0
while [[ $index -lt $1 ]]; do
    index=$(($index+1))
    seq=$index
    if [[ $seq -lt 10 ]]; then
        seq="0"$seq
    fi
    name=$base_name"_"$seq 
    ./genFlow.sh $name $index 
    # install flows
    ovs-ofctl add-flows ovs-lan $name".flows"
done

ovs-ofctl dump-flows ovs-lan -O OpenFlow13

