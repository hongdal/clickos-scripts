#!/bin/bash
#---------------------------------------------------------------------------
# If instance name does not exsist, the results are undefined!!
#---------------------------------------------------------------------------
if [[ -z $2 ]]; then
    echo "Usage: $0 <instance name> <instance seq number>"
    echo "E.g., $0 firewall_03 3 --> firewall_03 is associated to netns3 "
    exit 1
fi 

vm_port=`./getPort.py $1 | awk '{print $1}'`
seq=`expr $2 + $2 - 1`
client_addr="10.130.127.1"
server_addr="10.130.127.2"

sed 's/00,output:[1-9]*/00,output:'$vm_port'/' template.flows > tmp.flows
sed -i 's/,in_port=[1-9]*,/,in_port='$vm_port',/' tmp.flows 
sed -i 's/1,nw_dst=10\.130\.127\.[1-9]*,/1,nw_dst='$client_addr',/' tmp.flows 
sed -i 's/2,nw_dst=10\.130\.127\.[1-9]*,/2,nw_dst='$server_addr',/' tmp.flows
sed -i 's/10\.130\.127\.[1-9]*,actions=output:2/'$client_addr',actions=output:2/' tmp.flows
sed -i 's/10\.130\.127\.[1-9]*,actions=output:1/'$server_addr',actions=output:1/' tmp.flows 
sed -i 's/mod_dl_dst:00:00:00:00:0[1-9]:00/mod_dl_dst:00:00:00:00:0'$2':00/' tmp.flows

cat tmp.flows > $1".flows"
rm -f tmp.flows
