#!/bin/bash
#----------------------------------------------------------------------------------
# This script is used to create a single firewall instance and destroy it immediately.
# Modify it as you want to meet your requirements.
#----------------------------------------------------------------------------------

count=0
while [[ $count -lt 30 ]]; do 
    xl create firewall_01.xen 
    sleep 0.1
    cosmos start firewall_01 firewall_01.click 
    sleep 0.1 
    xl list
    sleep 0.1 
    xl destroy firewall_01
    count=$(($count+1))
    sleep 0.1 
done
