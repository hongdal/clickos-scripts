// Definition of virtual devices.
feth0 :: FromDevice(0);
feth1 :: FromDevice(1);
feth2 :: FromDevice(2);

// Classifier for interfaces. Avoid self loop
feth0 -> Classifier( 0/000000000100)[0] -> Print("eth0") -> Discard()
feth1 -> Classifier( 0/000000000101)[0] -> Print("eth1") -> Discard()
feth2 -> Classifier( 0/000000000102)[0] -> Print("eth2") -> Discard()

