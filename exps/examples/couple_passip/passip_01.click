// Definition of virtual devices.
feth0 :: FromDevice(0);
teth0 :: ToDevice(0);
feth1 :: FromDevice(1);
teth1 :: ToDevice(1);
feth2 :: FromDevice(2);
teth2 :: ToDevice(2);

// Definition of VNF_IN. Set dest MAC by default. 
VNF_IN :: {
    input[0] -> [0]output; // from eth0 
    input[1] -> [1]output; // from eth1 
    input[2] -> [2]output; // from eth2 
};

// Definition of VNF_OUT. set MAC address. 
VNF_OUT :: {
    input[0]->StoreEtherAddress(00:00:00:00:01:00,src)->StoreEtherAddress(a0:ec:f9:e8:ac:69,dst)->[0]output; // to eth0
    input[1]->StoreEtherAddress(00:00:00:00:01:01,src)->StoreEtherAddress(a0:ec:f9:e8:a4:72,dst)->[1]output; // to eth1
    input[2]->StoreEtherAddress(00:00:00:00:01:02,src)->StoreEtherAddress(74:a0:2f:5f:17:e4,dst)->[2]output; // to eth2
};

// Classifier for interfaces. Avoid self loop
feth0 -> Classifier( 0/000000000100)[0] -> [0]VNF_IN;
feth1 -> Classifier( 0/000000000101)[0] -> [1]VNF_IN;
feth2 -> Classifier( 0/000000000102)[0] -> [2]VNF_IN;

// from VNF_OUT to out device. 
VNF_OUT[0] -> teth0 
VNF_OUT[1] -> teth1 
VNF_OUT[2] -> teth2 

define ($eth0_src 00:00:00:00:01:00);
define ($eth0_dst a0:ec:f9:e8:ac:69);
define ($eth1_src 00:00:00:00:01:01);
define ($eth1_dst a0:ec:f9:e8:a4:72);
define ($eth2_src 00:00:00:00:01:02);
define ($eth2_dst 74:a0:2f:5f:17:e4);
//---------------------------------------------------//
//                VNF CODE STARTS HERE                
//---------------------------------------------------//
VNF_IN[0] -> [1]VNF_OUT // passthru
VNF_IN[1] -> [0]VNF_OUT // passthru
VNF_IN[2] -> Discard()  // discard 
Idle -> [2]VNF_OUT;
