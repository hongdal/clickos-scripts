#!/bin/bash
# timelist.result stores the time per packet. 
# Each packet is around 1494 bytes.
# The final units of the second conlum is Mbps

if [[ -z $1 ]]; then 
    echo "Usage: $0 <t|u>"
    exit 1
fi

# get process time first.
./getProcessTime.sh $1 > /dev/null
if [[ $? -ne 0 ]]; then 
    exit 1
fi
if [[ $1 == 't' ]]; then 
    fname1="timelist_tcp.result.01"
    fname2="timelist_tcp.result.02"
elif [[ $1 == 'u' ]]; then 
    fname1="timelist_udp.result.01"
    fname2="timelist_udp.result.02"
else
    echo "Usage: $0 <t|u>"
    exit 1
fi

rm -f throughput.tmp
# $1: number of rules, $2: throughput(bps),  $3: std(bps)
awk -F ',' '{printf"%.12g,%.12g,%.12g\n" ,$1, 1494*8*1000000000.0/$2, 1494*8*1000000000.0/$3}' $fname1  > throughput.tmp
awk -F ',' '{printf"%.12g,%.12g,%.12g\n" ,$1, 1494*8*1000000000.0/$2, 1494*8*1000000000.0/$3}' $fname2 >> throughput.tmp

cat throughput.tmp | sort -t ',' -k 1 -g
exit 1
cat throughput.tmp | sort -t ',' -k 1 -g | awk -F ',' 'BEGIN{r=0;s=0;e=0;i=0}{if(i<2){i+=1;r+=$1;s+=$2;e+=$3}else{printf("%.10g,%.10g,%.10g\n",r,s,e);r=$1;s=$2;e=$3;i=1}}END{printf("%.10g,%.10g,%.10g\n",r,s,e)}'





