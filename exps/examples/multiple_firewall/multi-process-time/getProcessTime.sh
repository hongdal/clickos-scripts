#!/bin/bash

if [[ -z $1 ]]; then 
    echo "Usage: $0 <t|u> "
    exit 1
elif [[ $1 == "t" ]]; then
    files_01=`ls *_01_tcp.time`
    files_02=`ls *_02_tcp.time`
    output="timelist_tcp.result"
elif [[ $1 == "u" ]]; then
    files_01=`ls *_01_udp.time`
    files_02=`ls *_02_udp.time`
    output="timelist_udp.result"
else 
    echo "Usage: $0 <t|u> "
    exit 1
fi

rm -f $output
rm -f $output".01"
rm -f $output".02"
rm -f tmp
for f in $files_01; do 
    base=`echo $f | cut -d '_' -f1`
    echo $base","`cat $f` >> $output".01" 
done
rm -f 02_timelist.result
for f in $files_02; do 
    base=`echo $f | cut -d '_' -f1`
    echo $base","`cat $f` >> $output".02"
done

cat  $output".01" > tmp
cat  $output".02" >> tmp

cat tmp | sort -t ',' -k 1 -g | awk -F ',' 'BEGIN{i=0;r=0;s=0;e=0}{if(i<2){i+=1;r+=$1;s+=$2;e+=$3}else{i=1;printf("%.10g,%.10g,%.10g\n",r,s,e); r=$1;s=$2;e=$3}}END{printf("%.10g,%.10g,%.10g\n",r,s,e)}'  > $output

# $1: number of rules,  $2: process time/packet (nanoseconds), $3: std
cat $output 
exit 0
