// Definition of virtual devices.
feth0 :: FromDevice(0);
teth0 :: ToDevice(0);
feth1 :: FromDevice(1);
teth1 :: ToDevice(1);
feth2 :: FromDevice(2);
teth2 :: ToDevice(2);

// Definition of VNF_IN. Set dest MAC by default. 
VNF_IN :: {
    input[0] -> [0]output; // from eth0 
    input[1] -> [1]output; // from eth1 
    input[2] -> [2]output; // from eth2 
};

// Definition of VNF_OUT. set MAC address. 
VNF_OUT :: {
    input[0]->StoreEtherAddress(00:00:00:00:03:00,src)->[0]output; // to traffic_out_0 
    input[1]->StoreEtherAddress(00:00:00:00:03:01,src)->[1]output; // to eth1
    input[2]->StoreEtherAddress(00:00:00:00:03:02,src)->[2]output; // to eth2
};

// Classifier for interfaces. Avoid self loop
feth0 -> Classifier( 0/000000000300)[0] -> [0]VNF_IN;
feth1 -> Classifier( 0/000000000301)[0] -> [1]VNF_IN;
feth2 -> Classifier( 0/000000000302)[0] -> [2]VNF_IN;

// from VNF_OUT to out device. 
VNF_OUT[0] -> teth0 
VNF_OUT[1] -> teth1 
VNF_OUT[2] -> teth2 

define ($eth0_src 00:00:00:00:02:00);
define ($eth0_dst a0:ec:f9:e8:ac:69);
define ($eth1_src 00:00:00:00:02:01);
define ($eth1_dst a0:ec:f9:e8:a4:72);
define ($eth2_src 00:00:00:00:02:02);
define ($eth2_dst 74:a0:2f:5f:17:e4);
//---------------------------------------------------//
//                VNF CODE STARTS HERE                
//---------------------------------------------------//
VNF_IN[1] -> Discard()
Idle -> [1]VNF_OUT
Idle -> [2]VNF_OUT
// VNF_IN[0] -- comes from eth0 , and so on. 
// VNF_OUT[1] -- to eth1 , and so on. 
//---------------- definition of classifier for arp/ip ---------------------
// traffic. 
traffic_c :: Classifier(
    12/0800,    // traffic from client and to clickos 
    -);
VNF_IN[0] -> traffic_c;    // from eth0
//-----------------------------------
traffic_out :: Classifier(
    6/7aba548f6409,     // from client, should go to server. 
    6/2e49d862c619,     // from server, should go to client. 
    -);
//traffic_out[0] -> StoreEtherAddress(2e:49:d8:62:c6:19,dst) -> Print("toServer") -> [0]VNF_OUT 
//traffic_out[1] -> StoreEtherAddress(7a:ba:54:8f:64:09,dst) -> Print("toClient") -> [0]VNF_OUT
traffic_out[0] -> StoreEtherAddress(2e:49:d8:62:c6:19,dst) -> [0]VNF_OUT 
traffic_out[1] -> StoreEtherAddress(7a:ba:54:8f:64:09,dst) -> [0]VNF_OUT
traffic_out[2] -> Discard() 
//-----------------------------------

// maintenance 
message_c      :: Classifier(
	12/0800 23/fd,      // IP test 
	12/0800 23/fe,      // IP test 
    -);
VNF_IN[2] -> message_c;    // from eth2 

//------------------ Initialization --------------------
RandomSource(1) -> Initglobal() -> Discard();
fwmatch :: firewallmatch();
fwman :: fwmanager(); 
//stime :: EvalTime("start");
//etime :: EvalTime("end");
//------------------ Start testing --------------------
// maintenance message 
message_c[0] -> CheckIPHeader(14) -> fwman -> Discard();    // control messages. 
message_c[1] -> [1]fwmatch;     // firewall is ready. 
message_c[2] -> Discard();

gettime :: GetTimeVal();
// traffic. 
traffic_c[0] -> SetTimeVal() -> CheckIPHeader(14) -> SimpleQueue(65536) -> [0]fwmatch;
//fwmatch[0] -> Print("Forwarded") -> [0]VNF_OUT; // to eth1
fwmatch[0] -> [0]gettime[0] -> Unqueue() -> traffic_out; // to eth1
fwmatch[1] -> Print("Dropped") -> Discard();
traffic_c[1] -> Discard();
Idle() -> [1]gettime[1] -> Discard()

