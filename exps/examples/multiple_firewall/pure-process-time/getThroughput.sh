#!/bin/bash
# timelist.result stores the time per packet. 
# Each packet is around 1494 bytes.
# The final units of the second conlum is Mbps

# $1: number of rules, $2: throughput(bps),  $3: std(bps)
awk -F ',' '{printf"%.12g,%.12g,%.12g\n" ,$1, 1494*8*1000000000.0/$2, 1494*8*1000000000.0/$3}' timelist.result | sort -t ',' -k 1 -g

