#!/bin/bash

files=`ls *.time`

rm -f timelist.result
for f in $files; do 
    base=`echo $f | cut -d '_' -f1`
    echo $base","`cat $f` >> timelist.result
done

# $1: number of rules,  $2: process time/packet (nanoseconds), $3: std
cat timelist.result
