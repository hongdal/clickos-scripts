#!/bin/bash

if [[ -z $1 ]]; then 
    echo "Usage: $0 <file base name>"
    echo "E.g., $0 400_tcp  --> input 400_tcp.txt, output 400_tcp.time"
    exit 1
fi

input=$1".txt"
output=$1".time"
if [[ ! -f $input ]]; then 
    echo "Could not find: $input"
    exit 1
fi

# pre-compute
avg=`grep -e '^@,[0-9]*,#' $input | awk -F ',' 'BEGIN{s=0;}{s+=$2}END{printf("%.10g",s/NR)}'`

echo "$avg"

# compute
grep -e '^@,[0-9]*,#' $input | awk -v val="$avg" -F ',' 'BEGIN{s=0;ss=0;ex=0;ex2=0;dx=0;i=0}{if($2<val*5){s+=$2;ss+=$2*$2;i+=1}}END{ex=s/i;ex2=ss/i;dx=ex2-ex;printf("%.10g,%.10g\n",ex,sqrt(dx))}' > $output 
cat $output
