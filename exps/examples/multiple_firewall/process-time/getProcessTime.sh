#!/bin/bash

if [[ -z $1 ]]; then 
    echo "Usage: $0 <t|u>"
    exit 1
elif [[ $1 == 'u' ]]; then 
    files=`ls *_udp.time`
elif [[ $1 == 't' ]]; then
    files=`ls *_tcp.time`
else
    echo "Usage: $0 <t|u>"
    exit 1
fi

rm -f timelist.result
for f in $files; do 
    base=`echo $f | cut -d '_' -f1`
    echo $base","`cat $f` >> timelist.result
done

# $1: number of rules,  $2: process time/packet (nanoseconds), $3: std
cat timelist.result | sort -t ',' -k 1 -g
