#!/bin/bash
#------------------------------------------------------------------
# Run this scritps. 
# Dependency:   
#   1) firewall.vnf
#   2) ../setup_clickos.sh 
#   3) ../make_nfv_click.sh 
#   4) ../make_nfv_xen.sh
#
# Output: 
#   1) firewall_01.click : filter traffic from client to server.
#   2) firewall_01.xen 
#   3) firewall_02.click : filter traffic from client to server.  
#   4) firewall_02.xen 
#   
#   Then this scritp will start two clickos instances, each of them 
#   are virtual firewall. The firewall names are: 
#   firewall_01 and firewall_02 respectively.  
# 
# Usage: 
#   ./couple_firewall.sh <client_ip> <server_ip> [local_ip]
#
# Author: 
# Hongda (hongdal@clemson.edu)
# 07/11/2015
#------------------------------------------------------------------

if [[ $# -lt 2 ]]; then 
    echo "Usage: $0 <client_ip> <server_ip> [local_ip]"
    exit 1
fi 

client=$1
server=$2
local_ip=$3
cpath=`pwd`
cd ../

# create firewall for traffic from client to server. 
./setup_clickos.sh 1 $cpath/firewall.vnf $cpath/firewall_01.xen $client $server $local_ip > $cpath/firewall_01.click 
# create firewall for traffic from server to client. 
./setup_clickos.sh 2 $cpath/firewall.vnf $cpath/firewall_02.xen $client $server $local_ip > $cpath/firewall_02.click 
# come back. 
cd $cpath

echo "Creating firewalls ...  "
# starting virtual firewall_01 
xl create firewall_01.xen 
`cosmos start firewall_01 firewall_01.click`
# starting virtual firewall_02 
xl create firewall_02.xen 
`cosmos start firewall_02 firewall_02.click`

# listing the results. 
sleep 2
xl list


