#!/bin/bash

name=$1
eth0=$2
bridge0=$3
eth1=$4
bridge1=$5
eth2=$6
bridge2=$7

if [[ -z "$eth0" ]]; then 
    echo "eth0 not found!" >& 2
    exit 1
fi
if [[ -z "$bridge0" ]]; then 
    echo "bridge0 not found!" >& 2
    exit 1
fi
if [[ -z "$eth1" ]]; then 
    echo "eth1 not found!" >& 2
    exit 1
fi
if [[ -z "$bridge1" ]]; then 
    echo "bridge1 not found!" >& 2
    exit 1
fi
if [[ -z "$eth2" ]]; then 
    eth2="ovs-lan"
    echo "eth2 use default: $eth2" >& 2
fi
if [[ -z "$bridge2" ]]; then 
    bridge2="ovs-lan"
    echo "bridge2 use default: $bridge2" >& 2
fi
#---------------------------------------------------------------#
#                   Output to stdout                            # 
#---------------------------------------------------------------#
echo "kernel = '/local/work/clickos/minios/build/clickos_x86_64'"
echo "vcpus = '1'"
echo "memory = '64'"

echo "vif = ['script=vif-openvswitch,mac=$eth0,bridge=$bridge0', 'script=vif-openvswitch,mac=$eth1,bridge=$bridge1', 'script=vif-openvswitch,mac=$eth2,bridge=$bridge2']"

echo "name = '$name'"
echo "on_crash = 'preserve'"





