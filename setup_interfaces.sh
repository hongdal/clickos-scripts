#!/bin/bash

if [[ -z $1 ]]; then
    echo "Usage: $0 <lan network id>"
    echo "$0 10.10.1.0 --> the lan network id could be found by 'route' "
    exit 1
fi

# set pass code for hongdal. In case wan is not accessable.  
passwd hongdal

# rename wan
int_name=`route | grep "default" | awk '{print $8}'`
echo "Shutting down $int_name ... "
ifconfig $int_name down
echo "Setting $int_name ... "
ip link set dev $int_name name wan0
echo "Turning on $int_name ... "
ifconfig wan0 up
echo "DHCP for $int_name ... "
dhclient wan0

# rename lan.
int_names=`route | grep "$1" | awk '{print $8}'`
index=0
for int_name in $int_names; do 
    echo "Shutting down $int_name ... " 
    ifconfig $int_name down 
    echo "Setting $int_name ... "
    ip link set dev $int_name name lan$index
    echo "Turning on lan$index ... " 
    ifconfig lan$index up
    index=$(($index+1))
done


