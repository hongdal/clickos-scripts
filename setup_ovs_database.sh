#!/bin/bash


sudo /sbin/modprobe openvswitch

# start database 
sudo ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock --remote=db:Open_vSwitch,Open_vSwitch,manager_options --private-key=db:Open_vSwitch,SSL,private_key --certificate=db:Open_vSwitch,SSL,certificate --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert --pidfile --detach
# init 
sudo ovs-vsctl --no-wait init
sudo ovs-vswitchd --pidfile --detach
for br in `ovs-vsctl show | grep Bridge | awk -F ' ' '{print $2}'`; do 
    ovs-vsctl del-br $br
done
echo "ovs-vsctl show: "
ovs-vsctl show


