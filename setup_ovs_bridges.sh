#!/bin/bash

# get input.
ovs_name=$1
if [[ -z $ovs_name ]]; then
	echo "Usage: $0 <ovs name> [interface to bind]"
	echo "e.g. $0 ovsbr0 lan0"
	exit 1
fi
int_name=$2
if [[ -z $int_name ]]; then
	echo "Use lan0 as default."
	int_name="lan0"
fi

# is dhcp ?
dhcp_pids=`ps aux | grep "dhclient $int_name" | grep -v "grep" | awk -F ' ' '{print $2}'`

# Not dhcp.
if [[ -z $dhcp_pids ]]; then 
	# get interface ip address and netmask.
	int_ip=`ifconfig $int_name | grep "inet addr" | awk -F ' ' '{print $2}'| awk -F ':' '{print $2}'`
	int_mask=`ifconfig $int_name | grep "Mask" | awk -F ' ' '{print $4}' | awk -F ':' '{print $2}'`
	# add bridge. 
	ovs-vsctl add-br $ovs_name 
	if [[ $? -ne 0 ]]; then 
        echo "WARNING: add bridge failed for : $ovs_name"
	fi
	# add port
	ovs-vsctl add-port $ovs_name $int_name 
	if [[ $? -ne 0 ]]; then
        echo "ERROR: add bridge failed for : $ovs_name"
        echo "Please check for bridge, $ovs_name"
		exit 1
	fi
	# clear old ip.
	ifconfig $int_name 0
	# set new ip. 
	ifconfig $ovs_name $int_ip netmask $int_mask up 
# it's dhcp
else
	# add bridge. 
	ovs-vsctl add-br $ovs_name 
	if [[ $? -ne 0 ]]; then 
        echo "WARNING: add bridge failed for : $ovs_name"
	fi
	# add port
	ovs-vsctl add-port $ovs_name $int_name 
	if [[ $? -ne 0 ]]; then
        echo "ERROR: add bridge failed for : $ovs_name"
        echo "Please check for bridge, $ovs_name"
		exit 1
	fi
    ifconfig $int_name 0
	# start new dhcp client.	
	dhclient $ovs_name
	# kill dhclients.
	for dhcp_pid in $dhcp_pids; do
		kill $dhcp_pid
	done
fi

# show results
ovs-vsctl show
echo "--------------------------------------------"
ifconfig $ovs_name
ifconfig $int_name

