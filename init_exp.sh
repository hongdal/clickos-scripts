#!/bin/bash

# setup ovs database. 
./setup_ovs_database.sh 

# setup interfaces. 
./setup_interfaces.sh

# create ovs bridges. 
echo "Creating ovs-wan on wan0 ... "
./setup_ovs_bridges.sh ovs-wan wan0 
echo "Creating ovs-lan lan0 ... "
./setup_ovs_bridges.sh ovs-lan lan0 
echo "Creating ovs-lan lan1 ... "
./setup_ovs_bridges.sh ovs-lan lan1 

echo "+-------------------------------------------------------+"
echo "| Complete. Please Run 'ifconfig' to check the network. |"
echo "+-------------------------------------------------------+"
echo ""
echo ""
