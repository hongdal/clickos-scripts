#!/bin/bash

[ -z $VNF_ADDR_METHOD ] && VNF_ADDR_METHOD="addr_eth"

[ $VNF_ADDR_METHOD == "encap_udp" ] && {
  echo "VNF_OUT:: {"

  i=0
  while true; do
    var=VNF_OUTPUT$i
    arg=${!var}
    [ -z "$arg" ] && break

    dst_ip=`echo $arg|tr ':' ' '|awk '{ print $1; }'`

    dst_port=`echo $arg|tr ':' ' '|awk '{ print $2; }'`
    echo "  input[$i] -> Socket(UDP, $dst_ip, $dst_port);"

    i=$(($i+1))
  done

  echo "};"

  echo "
VNF_IN :: Socket(UDP, 0.0.0.0, $VNF_LISTEN_PORT);
"

  cat $VNF_INFILE
  exit
}

#out_MAC=`ip a s $VNF_IFACE_OUT | grep 'link/ether' | awk '{print $2}'`
out_MAC=`grep mac config.xen | awk -F '=' '{print $4}' | awk -F ',' '{print $1}'`

echo "from_in_dev :: FromDevice($VNF_IFACE_IN);" #, KEYWORDS ALLOW_NONEXISTENT true);" # should we use PollDevice for performance?
echo "to_out_dev :: ToDevice($VNF_IFACE_OUT);" #, 8, KEYWORDS ALLOW_NONEXISTENT true);"
echo ""
echo "VNF_IN :: PushNull;"
echo ""
echo "VNF_OUT :: {"

i=0
while true; do
  var=VNF_OUTPUT$i
  arg=${!var}
  [ -z "$arg" ] && break

  dst_ip=`echo $arg|tr '/' ' '|awk '{ print $1; }'`
  dst_mac=`echo $arg|tr '/' ' '|awk '{ print $2; }'`

  echo "  input[$i] -> StoreEtherAddress($dst_mac, dst) -> [$i]output;"

  i=$(($i+1))
done

echo "};"

echo ""
echo "from_in_dev -> VNF_IN"
echo "VNF_OUT[0] -> StoreEtherAddress($out_MAC, src) -> SimpleQueue -> to_out_dev"

echo ""
echo "// VNF CODE STARTS HERE"
cat $VNF_INFILE
